package com.love.loveperccentage;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class LovePercentage extends AppCompatActivity implements
        OnClickListener {
    static final String AD_UNIT_ID = "ca-app-pub-8444650851472007/9448751377";

    AdView adView;
    LinearLayout linearLayout;
    String Name, love;
    EditText loverName, yourName;
    // TextView textView1,textView2;
    Button calButton, bt1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love_percentage);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setLogo(R.drawable.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        	setTitle("Love %");
        yourName = (EditText) findViewById(R.id.name_edt);
        loverName = (EditText) findViewById(R.id.email_edt);
        calButton = (Button) findViewById(R.id.register_btn);
        bt1 = (Button) findViewById(R.id.cancel_btn);
        bt1.setOnClickListener(this);
        calButton.setOnClickListener(this);

        // Admob Code
        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);
        linearLayout = (LinearLayout) findViewById(R.id.add_linera);
        linearLayout.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AD_UNIT_ID)
                .build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Exit");
        alert.setMessage("Do you want to exit?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alert.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_btn:
                Name = yourName.getText().toString();
                love = loverName.getText().toString();
                if (Name.length() < 2 || love.length() < 2) {
                    Toast.makeText(getApplicationContext(),
                            "Your name length minimum 2 Character",
                            Toast.LENGTH_LONG).show();
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() >= 9 || love.length() >= 9) {
                    Toast.makeText(getApplicationContext(),
                            "Your name length maximum 8 Character",
                            Toast.LENGTH_LONG).show();
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 2) {
                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "59%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 2) {
                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "58%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 3) {
                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "74%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 4) {
                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "92%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "61%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 7) {
                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "72%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "96%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "73%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "78%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "95%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "74%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 7) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "87%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "64%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "90%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "99%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "99%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 7) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "68%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "63%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "71%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "74%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "55%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "88%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 7) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "78%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "80%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "85%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "77%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "99%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 7) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "76%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "59%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "89%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "93%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "60%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "66%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 7) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "80%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "75%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "77%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "38%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "88%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "78%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "69%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "89%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "90%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "99%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 8 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "94%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "94%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "78%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "90%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "88%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "83%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "99%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 7 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "98%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 6 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "99%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 5 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "66%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 4 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "84%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 3 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "82%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 2) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "69%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 3) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "77%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 4) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "66%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 5) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "51%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 6) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "71%");
                    intent.putExtra("lover", love);


                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 7) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "58%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);

                } else if (Name.length() == 2 && love.length() == 8) {

                    Intent intent = new Intent(LovePercentage.this,
                            Calculation.class);
                    intent.putExtra("cal", "49%");
                    intent.putExtra("lover", love);
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);
                }

                break;
            case R.id.cancel_btn:
                finish();

            default:
                break;
        }

    }

}
